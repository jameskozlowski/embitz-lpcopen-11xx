Open both of these projects into EmBitz and build. The LPCOpen11xx is the LPCOpen platform and the LPCProject is a simple blinking demo included with LPCOpen

The LPCOpen platform can be downloaded from here: http://www.nxp.com/products/software-and-tools/hardware-development-tools/lpcxpresso-boards/lpcopen-software-development-platform-lpc11xx:LPCOPEN-SOFTWARE-FOR-LPC11XX

I started to use EmBitz (http://www.emblocks.org ) using a LPC-Link 2 with the j-link firmware and after some work got the debugging to work. Since I already had some projects that used LPCOpen I decided to port it over into EmBitz and get it working

I have tested this using the LPC1115/303 Xpresso board (www.nxp.com/lpcxpresso) but it should work with the LPC1114 and other LPC11xx chips, some modifications may need to be made to the project, defines and linker script.
